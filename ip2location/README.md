------
README
------
This module rquires the IP2Location LITE Edition which is a free package with accuracy up to Class C (192.168.1.X) only.
Please download it from the official website, https://lite.ip2location.com/ip2location-lite.

------
Database file
------

- Database file name: IP2LOCATION-LITE-DB11.BIN

- Database file size: 85.93 MiB

- After download, Copy the folder "ip2location" to Drupal root /libraries/ directory.
