<?php

/**
 * @file
 * Contains \Drupal\detect_ip_region\AddRegionSession.
 * Provide a service as an Event Subscriber to set the $_SESSION data of region. 
 */

namespace Drupal\detect_ip_region;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Provides AddRegionSession.
 */
class AddRegionSession implements EventSubscriberInterface
{

  /**
   * Sets extra HTTP headers.
   */
  public function onRespond(FilterResponseEvent $event)
  {
    // var_dump($event); return;
    if (!$event->isMasterRequest()) {
        return;
    }

    $response = $event->getResponse();

    $ip = $this->getRealIP();   

    // $session = \Drupal::service('session')->get('ip2location_8.8.8.8');
    $session = \Drupal::service('session')->get('ip2location_' . $ip);
    
    if(empty($session)){
      $result = $this->searchIp2Location($ip); 

      \Drupal::service('session')->set('ip2location_' . $ip, $result);
    }
    else {
      $result = $session;
      // var_dump($result);
    }

    if(!empty($result)) {
      if(isset($result['region_name'])) {
        \Drupal::service('session')->set('REGION',  $result['region_name']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents()
  {
      $events[KernelEvents::RESPONSE][] = ['onRespond'];
      return $events;
  }


  /**
   * Search Location GEO info on IP2Location database
   *
   * @param String $ip
   * @return array : GEO Location info
   */
  public function searchIp2Location($ip) {
    if(empty($ip)) {
      return false;
    }
    // $config = \Drupal::config('ip2location.settings');
    // $database_path = $config->get('database_path');
    // $cache_mode = $config->get('cache_mode');
    
    // $config = \Drupal::config('ip2location.settings');
    $database_path = 'libraries/ip2location/IP2LOCATION-LITE-DB11.BIN';
    $cache_mode = 'memory_cache';


    if (!is_file($database_path)) {
      // var_dump($database_path);
      return;
    }

    module_load_include('inc', 'detect_ip_region', 'src/IP2Location');

    switch ($cache_mode) {
        case 'memory_cache':
            $ip2location = new \IP2Location\Database($database_path, \IP2Location\Database::MEMORY_CACHE);
            break;

        case 'shared_memory':
            $ip2location = new \IP2Location\Database($database_path, \IP2Location\Database::SHARED_MEMORY);
            break;

        default:
            $ip2location = new \IP2Location\Database($database_path, \IP2Location\Database::FILE_IO);
    }

    $records = $ip2location->lookup($ip, \IP2Location\Database::ALL);
    // var_dump($records);

    return $this->wrapLocationDataObj($records, $ip);
  }

  /**
   * Help function to trim unavailable values
   *
   * @param array $location
   * @return array $newLocation
   */
  protected function wrapLocationData(array $location)
  {
      $newLocation = null;

      foreach ($location as $key => $value) {
          if (strpos($value, 'unavailable') == false) {
              $newLocation->$key = $value;
          }
      }
      return $newLocation;
  }

  /**
   * Help function to trim unavailable values
   *
   * @param array $location
   * @return json (Object) $newLocation 
   */

  protected function wrapLocationDataObj(array $records, $ip) {
    $raw = array(
      'ip_address' => $ip,
      'country_code' => $records['countryCode'],
      'country_name' => $records['countryName'],
      'region_name' => $records['regionName'],
      'city_name' => $records['cityName'],
      'latitude' => $records['latitude'],
      'longitude' => $records['longitude'],
      'zip_code' => $records['zipCode'],
      'time_zone' => $records['timeZone'],
    );

    return $raw;
    
  }

  /**
   * Help function to get a real IP address
   *
   * @return String: a real IP or defaul IP for local test
   */

  protected function getRealIP() {
    $ip = \Drupal::request()->getClientIp();

    if(empty($ip) || strstr($ip, '192.168.') || strstr($ip, '127.0.0')) {
      $ip = $_SERVER["REMOTE_ADDR"];      
      // print " 2: "; print_r($ip);
      if(empty($ip) || strstr($ip, '192.168.') || strstr($ip, '127.0.0')) {
        $ip = '70.24.251.81'; // canada ip address for local test       
      }
    }  

    return $ip;
  }


}