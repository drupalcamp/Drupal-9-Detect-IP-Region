<?php

namespace Drupal\detect_ip_region\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


/**
 * Controller routines for the test of delete_ip_region module.
 * 
 * Class PageController
 */
class PageController extends ControllerBase
{

    // use DescriptionTemplateTrait;

    /**
     * {@inheritdoc}
     */
    protected function getModuleName()
    {
        return 'detect_ip_region';
    }

    /**
     * Constructs a simple page.
     * 
     */
    public function simple()
    {
      $regionOrigin = \Drupal::service('session')->get('REGION');
      if (isset($_SESSION['REGION'])) {
        $region = $_SESSION['REGION'];
        // print_r($region);
      } else {
        $region = $regionOrigin;
      }

      // print "<pre>";
      // print_r($_SESSION);
      // print "</pre>"; 
      // exit();
      // canada.ca  70.24.251.81   

       
      return [
            '#markup' => '<p>' . $this->t('Debugging detect_ip_region module only:' ) . " region = $region". '</p>',
        ];
    }

    /**
     * Set the session data of region by getting the Form ChangeRegionForm
     */
    public function setRegionFormPage()
    {      
      // return [];
      $form = \Drupal::formBuilder()->getForm('\Drupal\detect_ip_region\Form\ChangeRegionForm');
      return $form;

    }

}
