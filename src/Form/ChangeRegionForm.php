<?php

/**
 * @file
 * Contains \Drupal\detect_ip_region\Form\ChangeRegionForm.
 */

namespace Drupal\detect_ip_region\Form;

// use Drupal\Component\Render\FormattableMarkup;
// use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\Core\Ajax\RedirectCommand;


/**
 * Class ChangeRegionForm.
 *
 * @package Drupal\detect_ip_region\Form
 */
class ChangeRegionForm extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'detect_ip_region_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $nojs = null)
    {
        // Add a custom library (JS). 
        $form['#attached']['library'][] = 'detect_ip_region/detect-ip-region';

        $form['#cache']['contexts'][] = 'session';

        // close the form (block).
        $form['close'] = [
            '#type' => 'button',
            '#value' => $this->t('Close'),
            '#attributes' => [
                'class' => ['close-cancel-region', 'close'],
            ],
 
        ];
        
        /**
         * Generate the URL path based on $_SESSION data.
         * Provide a ajax wrapper to change URL path.
         */


        $path = $this->getPathByRegion($this->getCurrentSessionRegion());
        $regionLink = "<span id=\"visit_region\">Visit Your Region Page</span>";

        $form['form_title'] = [
            '#type' => 'markup',
            '#markup' =>'<div class="form-title title form-item"><h4>Change Your Region or ' . $regionLink . '</h4></div>',
        ];

        // Ajax Search Box to search the Region by Postal Code
        $form['search'] = [
            '#type' => 'textfield',
            '#size' => 60,
            '#title' => $this->t('Search by Postal Code'),
            '#attributes' => ['class' => ['search-box', 'search']],
            '#ajax' => [
                'event' => 'change',
                'keypress' => true,
                'callback' => '::ajaxGetRegionFromPost',
                'wrapper' => 'message-wrapper',
                'effect' => 'fade',
                'progress' => [
                    'type' => 'throbber',
                    'message' => $this->t('Searching ...'),
                ],
            ],
        ];

        
        
        // Ajax wrapper container for ajax response.
        $form['messages'] = [
          '#type' => 'container',
          '#attributes' => ['id' => 'message-wrapper'],
        ];
        
        // $session = \Drupal::service('session')->get('ip2location_' . $ip);
        // print_r($session);
        $originRegion = $this->getOriginRegion();

        $form['region'] = [
            '#type' => 'radios',
            '#title' => $this->t('Select Your Region'),
            '#attributes' => ['class' => ['select-region']],
            '#options' => array(
                'Atlantic Canada' => $this->t('Atlantic Canada'),
                'Prairies/Territories' => $this->t('Prairies/ Territories'),
                'Ontario' => $this->t('Ontario'),
                'Quebec' => $this->t('Quebec'),
                'British Columbia/Yukon' => $this->t('British Columbia/Yukon'),
                // $originRegion =>'Origin Region: ' . $originRegion,
            ),

        ];

        // Add submit actions that handle the submission of the form.
        $form['actions'] = [
            '#type' => 'actions',
        ];

        // Add two ajax wrapper for the message and the URL path
        $form['actions']['button'] = [
            '#type' => 'button',
            '#value' => $this->t('Change Region'), 
            '#attributes' => [
                'class' => ['change-region', 'ajaxt-submit'],
            ],
            '#ajax' => [
                'callback' => '::ajaxSubmitForm',
                'wrapper' => ['message-wrapper', 'visit_region',],
                'progress' => [
                    'type' => 'throbber',
                    'message' => '...',
                ],
            ],
        ];

        // Cancle the form (block).
        //  $form['actions']['link'] = [
        //     // '#type' => 'button',
        //     // '#value' => $this->t('Visit Region Page'),
        //     '#markup' => l(t('Visit Region Page'), "/region/$path"),
        //     '#attributes' => [
        //         'class' => ['change-region', 'ajaxt-submit', 'visit-region'],
        //     ],
        // ];

        $regionLink = "<span class='wrapper-visit'><a href=\"/region/$path\" id=\"visit_region_button\" class=\"change-region\ \"visit-region\">Visit Region</a></span>";


        $form['actions']['request'] = [
            '#type' => 'markup',
            '#markup' => $regionLink,
            '#cache' => ['max-age' => 0],
        ];


        return $form;
    }

 
    /**
     * {@inheritdoc}
     * The submit is replaced by Ajax callback.
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // $oldSession = $this->getCurrentSessionRegion();
        // $newValue = $form_state->getValue('region');
        // if(isset($newValue)){
        //     $_SESSION['REGION'] = $newValue;
        //     $newSession = $_SESSION['REGION'];       
        //     $str = "You changed Region from <b>$oldSession</b> to <b>$newSession</b>";
        // }
        // else {
        //     $str = "Select a Region and try again!";
        // }
        // $this->messenger()->addStatus($this->t($str));
        // $form_state['rebuild'] = true;

    }


    /**
     * Implements the submit handler for the HtmlCommand AJAX call.
     * Set the SESSION date of region.
     * Replaces the url of "Visit your region page"
     * 
     * @param array $form
     *   Render array representing from.
     * @param FormStateInterface $form_state
     *   Current form state.
     *
     * @return AjaxResponse
     *   Array of AJAX commands to execute on submit of the modal form.
     */
    public function ajaxSubmitForm(array &$form, FormStateInterface $form_state)
    {
        $element = $form['messages'];
        // If the user submitted the form and there are errors, show them the
        // input dialog again with error messages. Since the title element is
        // required, the empty string wont't validate and there will be an error.
        if ($form_state->getErrors()) {
            // If there are errors, we can show the form again with the errors in
            // the status_messages section.
            $element['box'] = [
                '#type' => 'status_messages',
                '#weight' => -10,
            ];

           return $form['messages'];
        }
        // If there are no errors, show the output dialog.
        else {
            // We don't want any messages that were added by submitForm().
            $this->messenger()->deleteAll();
            $oldSession = $this->getCurrentSessionRegion();
            $newValue = $form_state->getValue('region');

            
            $response = new AjaxResponse();
            
            if(isset($newValue)){

                // Set the Session value
                $_SESSION['REGION'] = $newValue;
                $newSession = $_SESSION['REGION'];       
                $str = "You changed Region from <b>$oldSession</b> to <b>$newSession</b>";

                // Pass variable to drupalSettings of JS
                $response->addCommand(new HtmlCommand('#message-wrapper', $str));
                
                // Redirect to the region page
                $regionLink = $this->getPathByRegion($newValue);
                $response->addCommand( new RedirectCommand("/region/$regionLink"));

            }
            else {
                $str = "Select a Region and try again!";
                
                $response->addCommand(new HtmlCommand('#message-wrapper', $str ));

                // $regionLink = $this->getPathByRegion($newValue);
                // if($regionLink){
                //     $regionLinkHtml = "<a href=\"/region/$regionLink\" class=\"visit-region\">Visit Your Region Page</a>";
                //     $response->addCommand(new HtmlCommand('#visit_region', $regionLinkHtml));
                // }
                
            }

            return $response;

        }

    }

    /**
     * Ajax callback to search the Region by a Post Code
     * 
     * @param array &$form, 
     * @param FormStateInterface $form_state
     * @return string
     *  message of setting value.
     */

    public function ajaxGetRegionFromPost(array &$form, FormStateInterface $form_state)
    {
        $form['messages']['status'] = ['#type' => 'status_messages',];
        $str = "No Region found.";        
        
        $postcode = $form_state->getValue('search');
        if (strlen($postcode) < 1) {
            $str = "Please search again.";
        }
        else {
            

            $letter = substr($postcode, 0, 1);        
            // print $letter; echo; setMessage()
            $letter = strtoupper($letter);
            
            $provice = [];
            $provice["A"] = ["Newfoundland and Labrador" => 'Atlantic Canada'];
            $provice["B"] = ["Nova Scotia" => 'Atlantic Canada'];
            $provice["C"] = ["Prince Edward Island" => 'Atlantic Canada'];
            $provice["E"] = ["New Brunswick" => 'Atlantic Canada'];
            $provice["G"] = ["Eastern Quebec" => 'Quebec'];
            $provice["H"] = ["Metropolitan Montréal" => 'Quebec'];
            $provice["J"] = ["Western Quebec" => 'Quebec'];
            $provice["K"] = ["Eastern Ontario" => 'Ontario'];
            $provice["L"] = ["Central Ontario" => 'Ontario'];
            $provice["M"] = ["Metropolitan Toronto" => 'Ontario'];
            $provice["N"] = ["Southwestern Ontario" => 'Ontario'];
            $provice["P"] = ["Northern Ontario" => 'Ontario'];
            $provice["R"] = ["Manitoba" => 'Prairies/Territories'];
            $provice["S"] = ["Saskatchewan" => 'Prairies/Territories'];
            $provice["T"] = ["Alberta" => 'Prairies/Territories'];
            $provice["V"] = ["British Columbia" => 'British Columbia/Yukon'];
            $provice["Y"] = ["Yukon" => 'British Columbia/Yukon'];
            $provice["X"] = ["Northwest Territories and Nunavut" => 'Northwest Territories and Nunavut'];  
             
            if (isset($provice[$letter])) {
                $str = "";
                $arr = $provice[$letter];                
                foreach ($arr as $key => $value) {
                    $str .= "$key => $value Region";
                }           
                // $form['messages']['#value'] = $str;
            }
        }
        
        $form['messages']['#markup'] = $str;

        return $form['messages'];
    }

    /**
     * Get the current Session data of REGION
     * @param void
     * @return string: a SESSION data 
     */

    public function getCurrentSessionRegion(){
        if (isset($_SESSION['REGION'])) {
            $oldSession = $_SESSION['REGION'];
        } else {
            $oldSession = \Drupal::service('session')->get('REGION');
        }

        return (isset($oldSession)) ? $oldSession : "";



    }

    
    /**
     * Get the current Session data of REGION
     * @param void
     * @return string: a SESSION data 
     */

    public function getOriginRegion(){

      $session = \Drupal::service('session')->get('REGION');
      
      if (isset($session)) {
        return $session;
      }
      else {
        return "Region2";
      }

    }

    /**
     * Generate a url path by the Region Name
     * /region/atlantic-canada
     * /region/prairies-territories
     * /region/ontario
     * /region/quebec
     * /region/british-columbia-yukon  
     *
     * @param string $region
     * @return string: path of region
     */
    public function getPathByRegion(string $region=""){
        if ($region == "") {
            return false;
        }
        $str = str_replace(['/ ', ' ', '/'], ['-', '-', '-'], $region );
        return strtolower($str);
    }

} //END class
