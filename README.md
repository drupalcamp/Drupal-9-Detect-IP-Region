**Detect Region by IP address**

- Drupal 8 custom module;

- Detect the region of Canada; 

- Provide a form to reset the region.

- The free database IP2LOCATION-LITE-DB11.BIN is required.

- Please download the package from the official website, https://lite.ip2location.com/ip2location-lite.

- **Copy the folder "ip2location" to Drupal root /libraries/ directory after downloading and extracting.**
